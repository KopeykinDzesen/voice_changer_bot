import json

import requests
from django.http.response import HttpResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

import telepot
import os
import librosa
from matplotlib import pyplot as plt


class AkselView(View):
    def get(self, request, *args, **kwargs):
        return HttpResponse("Hello, Aksel!")


class HookHandler(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        message = json.loads(request.body)["message"]

        from pprint import pprint

        pprint(message)

        if not message.get("voice", None):
            return HttpResponse("OK")

        bot = telepot.Bot(os.environ.get("TELEGRAM_TOKEN"))

        file = bot.getFile(message["voice"]["file_id"])
        bot.download_file(message["voice"]["file_id"], file["file_path"])

        import subprocess

        subprocess.run(
            ["ffmpeg", "-i", file["file_path"], file["file_path"].replace("oga", "wav")]
        )

        plt.figure(figsize=(20, 10))
        data, sr = librosa.load(file["file_path"].replace("oga", "wav"))
        import pandas as pd
        import numpy as np

        data = np.array(data, dtype=float)
        print("-" * 100)
        print(data)
        print("-" * 100)
        plt.specgram(data, Fs=sr, NFFT=1024, noverlap=1000)
        image_path = f'image/image_{file["file_path"].split("/")[1].split("_")[1].replace("oga", "png")}'
        plt.savefig(image_path, bbox_inches="tight", cmap="RdBu")

        bot.sendPhoto(message["chat"]["id"], open(image_path, "rb"))

        return HttpResponse("OK")
