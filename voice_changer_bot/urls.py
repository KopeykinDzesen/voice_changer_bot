from django.contrib import admin
from django.urls import path
from aksel.views import AkselView, HookHandler

urlpatterns = [
    path("admin/", admin.site.urls),
    path("test/", AkselView.as_view()),
    path("hook/", HookHandler.as_view()),
]
